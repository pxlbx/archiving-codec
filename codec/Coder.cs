using System;
using Helpers;
using System.Collections.Generic;

namespace ARC{

    class Coder{

        /*  --------------------------- DEFAULT CODER PARAMETERS --------------------------------------- 
            
            BLOCK_LENGTH_DEFAULT    -> the default block length if not speficied
            PREDICTOR_SUPPRESSING_  -> the default suppressing threshold of the predictor if not specified.
            THRESHOLD_DEFAULT
             
            ------------------------------------------------------------------------------------------- */
        const byte BLOCK_LENGTH_DEFAULT = 16;
        const double PREDICTOR_SUPPRESSING_THRESHOLD_DEFAULT = 512;

        /*  --------------------------- ENCODER PARAMETERS ---------------------------------------------

            COMPRESS_BLOCKS         ->  tells the encoder to compress the blocks using an entropy coder
                                        (if true), otherwise writes the blocks uncompressed to the ARC file.
            BLOCK_LENGTH            ->  the length of every audio block
            WARM_UP_SAMPLES_COUNT   ->  the number of samples to be skipped by the prediction algorithm, 
                                        to serve as "warm-up" samples for the predictor.
            HTREE_POSTFIX_MERGER    ->  the unsigned byte that serves as the merging notation
                                        in the postfix expression of the huffman tree used in compressing 
                                        the BPE headers. This can be values from 18 - 255.
                                        Values 0 - 17 are possible BPE headers, and are therefore reserved.
            PREDICTOR_SUPPRESSING_  ->  the threshold for suppressing the predictor
            THRESHOLD
        
            -------------------------------------------------------------------------------------------- */
        
        bool COMPRESS_BLOCKS { get; }
        byte BLOCK_LENGTH, WARM_UP_SAMPLES_COUNT = 3, HTREE_POSTFIX_MERGER = 31;
        double PREDICTOR_SUPPRESSING_THRESHOLD { get; }

        public Coder(   byte b = BLOCK_LENGTH_DEFAULT, 
                        double st = PREDICTOR_SUPPRESSING_THRESHOLD_DEFAULT,
                        bool e = false    ){
                            
            BLOCK_LENGTH = b;
            PREDICTOR_SUPPRESSING_THRESHOLD = st;
            COMPRESS_BLOCKS = e;

        }

        public void Compress(string directory, string filename){
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("\nCompressing: " + directory + "\\" + filename);
            AudioData audio = FileHelper.GetAudioData(directory + "\\" + filename);
            audio.FilePath = directory + "\\" + filename.Substring(0, filename.Length - 4);
            FileHelper.PrintAudioInfo(audio);

            if(audio.NumberOfChannels == 1) MonoEncode(audio);
            else if(audio.NumberOfChannels == 2) StereoEncode(audio);
            else Console.WriteLine("Multichannel audio not supported.");
            Console.WriteLine("-----------------------------------------------------");
        }

        /*  The encoding algorithm for audio files with only 1 channel. */
        public void MonoEncode(AudioData audio){

            Console.WriteLine("Predicting samples and blocking errors...");
            BlockingData bdata = BlockErrors(Predict(NumHelper.ToIntArr(audio.Samples)));
            Console.WriteLine("Compressing Block Headers and Preparing ARC data");
            ARCData adata = PrepareARCData(bdata, audio);
            Console.WriteLine("Writing ARC Data...");
            WriteARCData(adata, audio.FilePath);
            Console.WriteLine("Done.");
            
        }

        /*  The encoding algorithm for audio files with 2 channels. */
        public void StereoEncode(AudioData audio){
            
            Console.WriteLine("Stereo encoding started...");
            Console.WriteLine("Running Interchannel Decorrelation...");
            DecorrelationData ddata = DecorrelateChannels(audio.Samples);
            Console.WriteLine("Predicting samples, blocking the residuals, and compressing block headers...");
            ARCData adata = PrepareARCData(BlockErrors(Predict(ddata.left)), BlockErrors(Predict(ddata.side)), audio);
            Console.WriteLine("Writing ARC data...");
            WriteARCData(adata, audio.FilePath);
            Console.WriteLine("Done.");

        }

        /*  return a struct that contains the left and side channels */
        DecorrelationData DecorrelateChannels(short[] samples){
            int[] left_channel = new int[samples.Length / 2];
            int[] side_channel = new int[samples.Length / 2];
            uint channel_index = 0;
            for(uint i = 0; i < samples.Length; i += 2){
                left_channel[channel_index] = samples[i];
                side_channel[channel_index] = samples[i] - samples[i + 1];
                channel_index++;
            }
            DecorrelationData c;
            c.left = left_channel;
            c.side = side_channel;
            return c;
        }

        /* Predicts a channel / series of samples and returns the prediction errors */
        int[] Predict(int[] samples){
            int[] residuals = new int[samples.Length - WARM_UP_SAMPLES_COUNT];
            for(uint i = WARM_UP_SAMPLES_COUNT; i < samples.Length; i++){
                int prediction = PredictSample(samples[i-1], samples[i-2], samples[i-3]);
                residuals[i - WARM_UP_SAMPLES_COUNT] = samples[i] - prediction;
            }
            return residuals;
        }
        
        /* Predicts the value of a sample based on the values of the previous 3 samples */
        int PredictSample(int prev1, int prev2, int prev3){
            int delta1 = prev1 - prev2;
            int delta2 = prev2 - prev3;

            double k = 0.6;   // default confidence factor

            if((delta1 > 0 && delta2 > 0) || (delta1 < 0 && delta2 < 0)){
                k = 1;
                int delta1_abs = Math.Abs(delta1);
                double suppression_ceiling = (PREDICTOR_SUPPRESSING_THRESHOLD * 10);
                
                //if delta1 exceeds the suppression ceiling, neutralize the prediction
                if(delta1_abs > suppression_ceiling) return prev1;

                k -= delta1_abs / suppression_ceiling;
            }
            return (int)(prev1 + (delta1 * k));
        }

        /* Divides the prediction errors into fixed-sized blocks */
        BlockingData BlockErrors(int[] prediction_errors){
            //  calculate number of blocks
            int b = prediction_errors.Length / BLOCK_LENGTH;
            if(prediction_errors.Length % BLOCK_LENGTH > 0) b += 1;

            //  the array that will hold the blocks
            Block[] blocks = new Block[b];
            uint block_ctr = 0;

            List<int> block_errors = new List<int>();
            block_errors.Capacity = BLOCK_LENGTH;

            int[] berrors = new int[BLOCK_LENGTH];
            int berr_ctr = 0;
            //  records the distribution of the BPEs
            ulong[] bpe_dist = new ulong[18];
            
            for(uint i = 0; i < prediction_errors.Length; i++){
                berrors[berr_ctr] = prediction_errors[i];
                berr_ctr++;
                if(berr_ctr == BLOCK_LENGTH || i + 1 == prediction_errors.Length){
                    blocks[block_ctr] = new Block(berrors);
                    bpe_dist[blocks[block_ctr].BitsPerError]++;
                    block_ctr++;
                    block_errors.Clear();
                    berr_ctr = 0;
                }
            }

            BlockingData bd;
            bd.Blocks = blocks;
            bd.BPEDistribution = bpe_dist;
            return bd;
        }

        /*  Prepares the data to be written to the ARC file (mono) */
        ARCData PrepareARCData(BlockingData bdata, AudioData audio){
            
            HuffmanCoder hcoder = new HuffmanCoder();
            //  BPEDistribution is a distribution array
            //  HTREE_POSTFIX_MERGER is an integer constant within the Coder class 
            HuffmanTree htree = hcoder.MakeHuffmanTree(bdata.BPEDistribution, HTREE_POSTFIX_MERGER);
            /*  A string array of the same length as the 
                distribution array that will hold the huffman codes */
            string[] hcodes = new string[bdata.BPEDistribution.Length];
            /*  Retrieveing the huffman codes, passing the string array */
            /*  String arrays in C# are, by default, passed by reference */
            hcoder.GetHuffmanCodes(htree, hcodes);
            
            //  set the huffman-coded BPE headers for each block
            foreach(var block in bdata.Blocks){
                block.SetBPECode(hcodes[block.BitsPerError]);
            }

            ARCData arc;
            /*  set the ARC header's info */
            arc.NumberOfSamples = (uint)audio.Samples.Length;
            arc.NumberOfChannels = (byte)audio.NumberOfChannels;
            arc.SamplingRate = audio.SamplingRate;
            arc.BitsPerSample = (byte)audio.BitsPerSample;
            arc.BlockCompressionEnabled = COMPRESS_BLOCKS;

            List<byte> postfix_in_bytes = new List<byte>();
            foreach(var s in htree.PostFixExpression.Split(' ')){
                postfix_in_bytes.Add(byte.Parse(s));
            }
            
            arc.BPE_HTREE_POSTFIX_1 = postfix_in_bytes.ToArray();
            arc.BPE_HTREE_POSTFIX_2 = null;
            
            arc.WarmUpSamples = new short[WARM_UP_SAMPLES_COUNT];
            for(int i = 0; i < WARM_UP_SAMPLES_COUNT; i++){
                arc.WarmUpSamples[i] = audio.Samples[i];
            }
            arc.Blocks = bdata.Blocks;

            return arc;
        }

        /*  Prepares the data to be written to the ARC file (stereo) */
        ARCData PrepareARCData(BlockingData bdata_left, BlockingData bdata_side, AudioData audio){
            
            HuffmanCoder hcoder = new HuffmanCoder();

            //  convert the BPE header of the left channel to huffman codes
            HuffmanTree htree_left = hcoder.MakeHuffmanTree(bdata_left.BPEDistribution, HTREE_POSTFIX_MERGER);
            string[] keys_left = new string[bdata_left.BPEDistribution.Length];
            hcoder.GetHuffmanCodes(htree_left, keys_left);
            
            //  set the huffman-coded BPE headers for each block of the left channel
            foreach(var block in bdata_left.Blocks){
                block.SetBPECode(keys_left[block.BitsPerError]);
            }

            //  convert the BPE header of the side channel to huffman codes
            HuffmanTree htree_side = hcoder.MakeHuffmanTree(bdata_side.BPEDistribution, HTREE_POSTFIX_MERGER);
            string[] keys_side = new string[bdata_side.BPEDistribution.Length];
            hcoder.GetHuffmanCodes(htree_side, keys_side);

            //  set the huffman-coded BPE headers for each block of the side channel
            foreach(var block in bdata_side.Blocks){
                block.SetBPECode(keys_side[block.BitsPerError]);
            }

            //  prepare the postfix trees in bytes (LEFT)
            List<byte> postfix_in_bytes_left = new List<byte>();
            foreach(var s in htree_left.PostFixExpression.Split(' ')){
                postfix_in_bytes_left.Add(byte.Parse(s));
            }

            //  prepare the postfix trees in bytes (SIDE)
            List<byte> postfix_in_bytes_side = new List<byte>();
            foreach(var s in htree_side.PostFixExpression.Split(' ')){
                postfix_in_bytes_side.Add(byte.Parse(s));
            }

            Console.WriteLine(htree_left.PostFixExpression);
            Console.WriteLine(htree_side.PostFixExpression);

            ARCData arc;
            /*  set the ARC header's info */
            arc.NumberOfSamples = (uint)audio.Samples.Length;
            arc.NumberOfChannels = (byte)audio.NumberOfChannels;
            arc.SamplingRate = audio.SamplingRate;
            arc.BitsPerSample = (byte)audio.BitsPerSample;
            arc.BlockCompressionEnabled = COMPRESS_BLOCKS;

            arc.BPE_HTREE_POSTFIX_1 = postfix_in_bytes_left.ToArray();
            arc.BPE_HTREE_POSTFIX_2 = postfix_in_bytes_side.ToArray();

            arc.WarmUpSamples = new short[WARM_UP_SAMPLES_COUNT * arc.NumberOfChannels];
            for(int i = 0; i < WARM_UP_SAMPLES_COUNT * arc.NumberOfChannels; i++){
                arc.WarmUpSamples[i] = audio.Samples[i];
            }

            arc.Blocks = new Block[bdata_left.Blocks.Length + bdata_side.Blocks.Length];

            //interlave the LEFT and SIDE blocks
            int blockindex = 0;
            for(int i = 0; i < arc.Blocks.Length; i += 2){
                arc.Blocks[i] = bdata_left.Blocks[blockindex];
                blockindex++;
            }
            blockindex = 0;
            for(int i = 1; i < arc.Blocks.Length; i += 2){
                arc.Blocks[i] = bdata_side.Blocks[blockindex];
                blockindex++;
            }
            
            return arc;
        }

        /*  Writes the ARC data to an ARC file */
        void WriteARCData(ARCData arc, string source_path){
            //  create a new ARC file in the outputs folder
            ARCWriter swriter = new ARCWriter(source_path + ".arc");

            Console.WriteLine("Writing ARC header...");
            //  write the ARC header
            swriter.Write(arc.NumberOfSamples);
            swriter.Write(arc.NumberOfChannels);
            swriter.Write(arc.SamplingRate);
            swriter.Write(arc.BitsPerSample);

            //  write the BlockCompression flag (boolean) as a byte
            if(arc.BlockCompressionEnabled) swriter.Write((byte) 1);
            else swriter.Write((byte) 0);

            //  write the BPE Postfix Tree(s)
            if(arc.NumberOfChannels == 1){
                swriter.Write((byte)arc.BPE_HTREE_POSTFIX_1.Length);
                foreach(var b in arc.BPE_HTREE_POSTFIX_1){
                    swriter.Write(b);
                }
            }
            else{
                swriter.Write((byte)arc.BPE_HTREE_POSTFIX_1.Length);
                foreach(var b in arc.BPE_HTREE_POSTFIX_1){
                    swriter.Write(b);
                }
                swriter.Write((byte)arc.BPE_HTREE_POSTFIX_2.Length);
                foreach(var b in arc.BPE_HTREE_POSTFIX_2){
                    swriter.Write(b);
                }
            }

            //write the warm-up samples
            foreach(var sample in arc.WarmUpSamples){
                swriter.Write(sample);
            }

            Console.WriteLine("Writing blocks...");
            //  write the blocks
            foreach(var block in arc.Blocks){
                //  write the block BPE header
                foreach(var bit in block.BPECode){
                    swriter.Write(bit);
                }
                
                if(block.BitsPerError > 0){
                    //  write the error magnitudes
                    foreach(var errorm in block.ErrorMagnitudes){
                        foreach(var bit in errorm.ToBooleanArray(block.BitsPerError)){
                            swriter.Write(bit);
                        }
                    }
                    //write the sign bits
                    foreach(var sign in block.ErrorSignBits){
                        swriter.Write(sign);
                    }
                }
                
                
            }

            swriter.Flush();
            swriter.BinStream.Dispose();
        }

    }

    class Block{
        /*  --------------------------- BLOCK PROPERTIES ----------------------------------------------

            BitsPerError        -> the BPE header. If the block is uncompressed, this tells the decoder
                                   the fixed bit length for each error values.
            BPECode            -> the Huffman-coded BPE Header, expressed as a series of boolean values (bits). 
                                   This is what is written to the compresed stream. 
            ErrorMagnitudes     -> the errors array, containing the *absolute values* of the errors.
            ErrorSignBits       -> the signs array, indicating the sign of each of the *non-zero* 
                                   values of the errors array.
            UniqueValues        -> the number of unique error magnitude values for a block,
                                   might be a useful information in compressing the block further

            ------------------------------------------------------------------------------------------- */
        public byte BitsPerError { get; }
        public bool[] BPECode { get; set; }
        public uint[] ErrorMagnitudes { get; }
        public bool[] ErrorSignBits { get; }
        public byte UniqueValues { get; set; }
        
        public Block(int[] errors){
            //  the array that contains the magnitude of the error values
            ErrorMagnitudes = new uint[errors.Length];
            //  the list of error signs
            List<bool> s = new List<bool>();
            //  set the capacity of the signs list to optimize insertion performance
            s.Capacity = errors.Length;
        
            uint max = 0;
            for(int i = 0; i < errors.Length; i++){
                ErrorMagnitudes[i] = (uint)Math.Abs(errors[i]);
                if(ErrorMagnitudes[i] > max) max = ErrorMagnitudes[i];
                //  separating the sign of non-zero error values
                if(errors[i] < 0)  s.Add(true);
                else if(errors[i] > 0) s.Add(false); 
            }
            //  the list that contains the error signs for the block
            ErrorSignBits = s.ToArray();
            //  the number of bits for each error magnitude 
            //  for writing the block later on as binary information
            BitsPerError = BinaryHelper.SizeInBits(max);
        }
        
        public void SetBPECode(string code){
            BPECode = new bool[code.Length];
            for(int i = 0; i < code.Length; i++){
                if(code[i] == '1') BPECode[i] = true;
                else BPECode[i] = false;
            }
        }
    }

    struct ARCData{
        /*  --------------------------- ARC Data Contents ----------------------------------------------
            
            The object that represents the compressed audio data to be written to an ARC file.

            NumberofSamples         ->  the number of samples in the audio data.
            NumberOfChannels        ->  The number of channels of the audio data.
            SamplingRate            ->  the sampling rate of the audio data.
            BitsPerSample           ->  the bit depth of the audio data.
            BlockCompressionEnabled ->  the overall data compression flag. if 1 (true), signals the decoder that
                                        some of the audio blocks are compressed using an
                                        entropy coding technique, and always reads the compression flag of a block
                                        before decoding the block's data.
                                        If BlockCompression is set to 0 (false), 
                                        the decoder does not read a compression flag before decoding a block
                                        because all blocks are expected to have been written uncompressed.
            BPE_HTREE_POSTFIX_1     ->  The postfix expression (as a series of bytes) that represents
                                        the Huffman tree used in coding the BPE headers. Each byte is
                                        a node of the tree. This is for one channel only.
            BPE_HTREE_POSTFIX_2     ->  Same as BPE_HTREE_POSTFIX. Used for the second huffman tree if the
                                        audio is stereo. This is for the side-channel huffman tree. If audio is mono,
                                        this is set to null and is not written to the compressed stream.
            WarmUpSamples           ->  the warm up samples for the prediction
            Blocks                  ->  the series of audio blocks, either mono or stereo (interleaved blocks).
                                    
            ------------------------------------------------------------------------------------------- */
        
        public uint NumberOfSamples;
        public byte NumberOfChannels;
        public uint SamplingRate;
        public byte BitsPerSample;
        public bool BlockCompressionEnabled;
        public byte[] BPE_HTREE_POSTFIX_1;
        public byte[] BPE_HTREE_POSTFIX_2;
        public short[] WarmUpSamples;
        public Block[] Blocks;

    }

    struct BlockingData{
        public ulong[] BPEDistribution;
        public Block[] Blocks;
    }

    struct DecorrelationData{
        public int[] left;
        public int[] side;
    }

}