using System;
using Helpers;
using WavLib;

namespace ARC{
    class Decoder{

        /*  --------------------------- ENCODER PARAMETERS ---------------------------------------------

            BLOCK_LENGTH            ->  the length of every audio block (same as encoder)
            WARM_UP_SAMPLES_COUNT   ->  the number of samples to be skipped by the prediction algorithm, 
                                        to serve as "warm-up" samples for the predictor.
            HTREE_POSTFIX_MERGER    ->  the unsigned byte that serves as the merging notation
                                        in the postfix expression of the huffman tree used in compressing 
                                        the BPE headers. This can be values from 18 - 255.
                                        Values 0 - 17 are possible BPE headers, and are therefore reserved.
            PREDICTOR_SUPPRESSING_  ->  the threshold for suppressing the predictor
            THRESHOLD
        
            -------------------------------------------------------------------------------------------- */

        byte BLOCK_LENGTH = 16, WARM_UP_SAMPLES_COUNT = 3, HTREE_POSTFIX_MERGER = 31;
        const double PREDICTOR_SUPPRESSING_THRESHOLD = 512;
        /*  The decompressor function, accepts a directory and a filename under the directory */
        public void Decompress(string directory, string filename){
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("Decompressing: " + directory + "\\" + filename);
            ARCReader ArcFile = new ARCReader(directory + "\\" + filename);
            ARCHeaderData header = ReadARCHeader(ArcFile);
            //  Decode ARC and write a mono WAVE file data
            if(header.NumberOfChannels == 1) WriteWAV(DecodeSamplesMono(ArcFile, header), false, directory, filename); 
            //  Decode ARC and write a stereo WAVE file data
            else if(header.NumberOfChannels == 2) WriteWAV(DecodeSamplesStereo(ArcFile, header), true, directory, filename);
            //  Dispose the stream of the ARC file
            ArcFile.BinStream.Dispose();
            Console.WriteLine("-----------------------------------------------------");
        }

        ARCHeaderData ReadARCHeader(ARCReader ArcFile){
            
            ARCHeaderData header;
            header.NumberOfSamples = ArcFile.ReadUint32();
            header.NumberOfChannels = ArcFile.ReadByte();
            header.SamplingRate = ArcFile.ReadUint32();
            header.BitsPerSample = ArcFile.ReadByte();

            if(ArcFile.ReadByte() == 0) header.BlockCompressionEnabled = false;
            else header.BlockCompressionEnabled = true;

            Console.WriteLine("Number of Samples: " + header.NumberOfSamples);
            Console.WriteLine("Number of Channels: " + header.NumberOfChannels);
            Console.WriteLine("Sampling Rate: " + header.SamplingRate);
            Console.WriteLine("Bit Depth: " + header.BitsPerSample + " bits / sample");
            Console.WriteLine("Block Compression Flag: " + header.BlockCompressionEnabled);

            //  read the postfix trees
            byte postfix1_length = ArcFile.ReadByte();
            header.BPE_HTREE_POSTFIX_1 = ArcFile.ReadBytes(postfix1_length);

            if(header.NumberOfChannels == 2){
                byte postfix2_length = ArcFile.ReadByte();
                header.BPE_HTREE_POSTFIX_2 = ArcFile.ReadBytes(postfix2_length);
            }
            else header.BPE_HTREE_POSTFIX_2 = null;

            //read the warm-up samples
            header.WarmUpSamples = new short[WARM_UP_SAMPLES_COUNT * header.NumberOfChannels];
            for(int i = 0; i < header.WarmUpSamples.Length; i++){
                header.WarmUpSamples[i] = ArcFile.ReadShort();
            }
            
            return header;
        }

        /* Given an ARC file with mono data, decodes the interleaved blocks of the single channel */
        short[] DecodeSamplesMono(ARCReader ArcFile, ARCHeaderData header){

            //  Reconstruct the BPE Huffman Tree
            HuffmanCoder hcoder = new HuffmanCoder();
            HuffmanTree htree = hcoder.HuffmanTreeFromPostFix(header.BPE_HTREE_POSTFIX_1, HTREE_POSTFIX_MERGER);
            string[] c = new string[18];
            hcoder.GetHuffmanCodes(htree, c);
            
            Console.WriteLine("Retrieving Samples...");

            //  include the warm up samples in the samples array
            short[] samples = new short[header.NumberOfSamples];
            for(int i = 0; i < header.WarmUpSamples.Length; i++){
                samples[i] = header.WarmUpSamples[i];
            }

            uint sampindex = WARM_UP_SAMPLES_COUNT;
            while(ArcFile.BinStream.Length != ArcFile.BinStream.Position){
                //read the BPE code to know the block's bits per error
                byte bpe = ReadBlockHeader(ArcFile, c);
                
                //read the errors and signs
                int[] errors = ReadErrors(ArcFile, bpe, BLOCK_LENGTH);
                for(int i = 0; i < errors.Length; i++){
                    if(errors[i] != 0){
                        if(ArcFile.ReadBit() == true) errors[i] = -errors[i];
                    }
                    //  run prediction function for current error
                    samples[sampindex] = (short)(PredictSample(samples[sampindex - 1], 
                                                                samples[sampindex - 2], 
                                                                samples[sampindex - 3]) + errors[i]);
                    sampindex++;
                    if(sampindex == samples.Length) break;
                }
            }

            return samples;
        }

        /*  Given an ARC file with stereo data, decodes the interleaved blocks of left and side channel */
        short[] DecodeSamplesStereo(ARCReader ArcFile, ARCHeaderData header){

            HuffmanCoder hcoder = new HuffmanCoder();
            //  reconstruct the Huffman tree for the left channel
            HuffmanTree htree_left = hcoder.HuffmanTreeFromPostFix(header.BPE_HTREE_POSTFIX_1, HTREE_POSTFIX_MERGER);
            string[] c_left = new string[18];
            hcoder.GetHuffmanCodes(htree_left, c_left);
            //  reconstruct the Huffman tree for the side channel
            HuffmanTree htree_side = hcoder.HuffmanTreeFromPostFix(header.BPE_HTREE_POSTFIX_2, HTREE_POSTFIX_MERGER);
            string[] c_side = new string[18];
            hcoder.GetHuffmanCodes(htree_side, c_side);
            
            short[] left = new short[header.NumberOfSamples / 2];
            int[] side = new int[header.NumberOfSamples / 2];

            //  include the warm up samples in the samples array
            for(int w = 0; w < header.WarmUpSamples.Length; w += 2){
                left[w] = header.WarmUpSamples[w];
                side[w] = header.WarmUpSamples[w] - header.WarmUpSamples[w + 1];
            }

            int i = 0;
            //  separate sample indexer for left and side channel samples
            uint sampindex_left = WARM_UP_SAMPLES_COUNT, sampindex_side = WARM_UP_SAMPLES_COUNT;
            //  flag for which channel the current block belongs
            bool LEFT_CHANNEL = false;

            while(ArcFile.BinStream.Length != ArcFile.BinStream.Position){
                byte bpe = 0;
                if(!LEFT_CHANNEL){ 
                    //  use the left channel's huffman table to read block header
                    bpe = ReadBlockHeader(ArcFile, c_left);
                    LEFT_CHANNEL = true;
                }
                else{ 
                    //  use the side channel's huffman table to read block header
                    bpe = ReadBlockHeader(ArcFile, c_side); 
                    LEFT_CHANNEL = false;
                }

                if(bpe > 0){
                    //  read the error stream of the block
                    int[] errors = ReadErrors(ArcFile, bpe, BLOCK_LENGTH);
                    for(int e = 0; e < errors.Length; e++){
                        // read the sign bits and adjust error values
                        if(errors[e] != 0){
                            bool sign = ArcFile.ReadBit();
                            if(sign) errors[e] = -errors[e];
                        }
                        if(LEFT_CHANNEL){
                            //  predict the current sample of left channel using the prediction function
                            left[sampindex_left] = (short)(PredictSample(left[sampindex_left - 1], 
                                                                        left[sampindex_left - 2], 
                                                                        left[sampindex_left - 3]) + errors[e]);
                            sampindex_left++;
                            if(sampindex_left == left.Length) break;
                        }
                        else{
                            //  predict the current sample of side channel using the prediction function
                            side[sampindex_side] = (short)(PredictSample(side[sampindex_side - 1], 
                                                                        side[sampindex_side - 2], 
                                                                        side[sampindex_side - 3]) + errors[e]);
                            sampindex_side++;
                            if(sampindex_side == side.Length) break;
                        }   
                    }
                }
                else{
                    //  if bpe = 0, skip this block (default value for array element is 0)
                    if(LEFT_CHANNEL) sampindex_left += BLOCK_LENGTH;
                    else sampindex_side += BLOCK_LENGTH;

                    if(sampindex_side >= side.Length) break;
                }
                
                i++;
            }

            //  reverse the interchannel decorrelation process 
            //  to retrieve samples for right channel
            short[] right = new short[left.Length];
            for(int r = 0; r < left.Length; r++){
                right[r] = (short)-(side[r] - left[r]);
            }
            //  interleave the left and right samples
            return Interleave(left, right);
        }
        
        /*  interleaves the left and right samples to ready for writing */
        short[] Interleave(short[] left, short[] right){
            short[] s = new short[left.Length + right.Length];
            uint sampindex = 0;
            for(int i = 0; i < s.Length; i += 2){
                s[i] = left[sampindex];
                s[i + 1] = right[sampindex];
                sampindex++;
            }
            return s;
        }

        /* writes the output WAV file */
        void WriteWAV(short[] samples, bool isStereo, string directory, string filename){
            WAVFile wav = new WAVFile();
            wav.Create(directory + "\\" + filename.Substring(0, filename.Length - 4) + ".wav", isStereo, 44100, 16, true);
            foreach(var sample in samples){
                wav.AddSample_16bit(sample);
            }
            wav.Close();
        }

        /*  the Prediction function (same as encoder) */
        int PredictSample(int prev1, int prev2, int prev3){
            int delta1 = prev1 - prev2;
            int delta2 = prev2 - prev3;

            double k = 0.6;   // default confidence factor

            if((delta1 > 0 && delta2 > 0) || (delta1 < 0 && delta2 < 0)){
                k = 1;
                int delta1_abs = Math.Abs(delta1);
                double suppression_ceiling = (PREDICTOR_SUPPRESSING_THRESHOLD * 10);
                
                if(delta1_abs > suppression_ceiling) return prev1;
                k -= delta1_abs / suppression_ceiling;
            }
            return (int)(prev1 + (delta1 * k));
        }

        /*  reads the BPE header of the block given a Huffman table */
        byte ReadBlockHeader(ARCReader ArcFile, string[] bpe_codes){
            string bits = "";
            bool bpe_match = false;
            while(!bpe_match){
                bool bit = ArcFile.ReadBit();
                if(bit == false) bits = bits + "0";
                else bits = bits + "1";
                for(byte i = 0; i < bpe_codes.Length; i++){
                    if(bpe_codes[i].Equals(bits)) return i;
                }
            }
            return HTREE_POSTFIX_MERGER;
        }

        /*  reads the error stream of the block given a Huffman table and the block's BPE */
        int[] ReadErrors(ARCReader ArcFile, byte bpe, uint errcount){
            int[] errors = new int[errcount];
            if(bpe > 0){
                for(int i = 0; i < errors.Length; i++){
                    bool[] error = new bool[bpe];
                    for(int b = 0; b < error.Length; b++){
                        error[b] = ArcFile.ReadBit();
                    }
                    errors[i] = BinaryHelper.BoolArrayToInt(error);
                }
            }
            return errors;
        }

    }

    struct ARCHeaderData{
        public uint NumberOfSamples;
        public byte NumberOfChannels;
        public uint SamplingRate;
        public byte BitsPerSample;
        public bool BlockCompressionEnabled;
        public byte[] BPE_HTREE_POSTFIX_1;
        public byte[] BPE_HTREE_POSTFIX_2;
        public short[] WarmUpSamples;
    }
}