using System.IO;
using System;

class ARCReader{

    public Stream BinStream;
    public bool[] BitBuffer = new bool[8];
    public int BitBufferIndex = 8;

    public ARCReader(string filepath){
        BinStream = File.Open(filepath, FileMode.Open);
    }

    public byte[] ReadBytes(int count){
        byte[] bytes = new byte[count];
        BinStream.Read(bytes, 0, count);
        return bytes; 
    }

    public uint ReadUint32(){
        return BitConverter.ToUInt32(ReadBytes(4), 0);
    }

    public short ReadShort(){
        return BitConverter.ToInt16(ReadBytes(2), 0);
    }

    public byte ReadByte(){
        return ReadBytes(1)[0];
    }

    public bool ReadBit(){
        if(BitBufferIndex == 8){
            BitBuffer = ByteToBoolArray(ReadByte());
            BitBufferIndex = 0;
        }
        bool bit = BitBuffer[BitBufferIndex];
        BitBufferIndex++;
        return bit;
    }

    private static bool[] ByteToBoolArray(byte b){
        // prepare the return result
        bool[] result = new bool[8];
        // check each bit in the byte. if 1 set to true, if 0 set to false
        for (int i = 0; i < 8; i++)
            result[i] = (b & (1 << i)) == 0 ? false : true;
        // reverse the array
        Array.Reverse(result);
        return result;
    }
}