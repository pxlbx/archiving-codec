using System;
using System.IO;

class ARCWriter{

    byte[] ByteBuffer = new byte[5000000];
    bool[] BitBuffer = new bool[8];
    public Stream BinStream;

    int ByteBufferSize = 0;
    byte BitBufferSize = 0;

    public ARCWriter(string filepath){
        BinStream = File.Open(filepath, FileMode.Create);
    }
    public void Write(bool bit){
        BitBuffer[BitBufferSize] = bit;
        BitBufferSize++;
        CheckBitBuffer();
    }

    public void Write(uint n){
        foreach(var b in BitConverter.GetBytes(n)){
            ByteBuffer[ByteBufferSize] = b;
            ByteBufferSize++;
            CheckByteBuffer();
        }
    }

    public void Write(short n){
        foreach(var b in BitConverter.GetBytes(n)){
            ByteBuffer[ByteBufferSize] = b;
            ByteBufferSize++;
            CheckByteBuffer();
        }
    }

    public void Write(byte b){
        ByteBuffer[ByteBufferSize] = b;
        ByteBufferSize++;
        CheckByteBuffer();
    }

    void CheckBitBuffer(){
        if(BitBufferSize == 8){
            ByteBuffer[ByteBufferSize] = BoolArrayToByte(BitBuffer);
            BitBufferSize = 0;
            ByteBufferSize++;
            CheckByteBuffer();
        }
    }

    void CheckByteBuffer(){
        if(ByteBufferSize == ByteBuffer.Length){
            BinStream.Write(ByteBuffer, 0, ByteBufferSize);
            ByteBufferSize = 0;
        }
    }

    public void Flush(){
        if(BitBufferSize != 0){
            ByteBuffer[ByteBufferSize] = BoolArrayToByte(BitBuffer);
            ByteBufferSize++;
            BitBufferSize = 0;
        }
        BinStream.Write(ByteBuffer, 0, ByteBufferSize);
        ByteBufferSize = 0;
        BinStream.Flush();
    }

    private byte BoolArrayToByte(bool[] source){
        byte result = 0;
        // This assumes the array never contains more than 8 elements!
        int index = 8 - source.Length;
        // Loop through the array
        foreach (bool b in source){
            // if the element is 'true' set the bit at that position
            if (b)
                result |= (byte)(1 << (7 - index));

            index++;
        }
        return result;
    }

}