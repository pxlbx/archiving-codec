# Archiving Codec

A prototype lossless audio coder and decoder (codec) created as part of my undergraduate thesis in the University of the Philippines Cebu.

This prototype audio codec compresses a WAVE audio file (.wav) into a proprietary audio format called ARC (.arc). ARC uses several techniques to achieve lossless compression - linear prediction of samples, inter-channel decorrelation for stereo inputs, and Huffman Coding.

The program was made using DotNet Core, and is written in C#. It also bundles a waveform visualizer that uses OpenTK (C# wrapper of OpenGL).
