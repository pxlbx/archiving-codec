using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace WaveVisualizer{
    public class Visualizer : GameWindow{
        public List<VisualizerInput> Inputs = new List<VisualizerInput>();
        public bool[] SampleMarkers = null;
        public long SampleStart = 0;
        public int SamplesPerGlance = 200, WaveFormAmpFactor = 3, HorizontalPanFactor = 300;
        public float DistanceBetweenSamples = 0.03f;
        public Visualizer() : base(1000, 700, GraphicsMode.Default, "Audio Compression"){
            VSync = VSyncMode.On;
        }

        protected override void OnLoad(EventArgs e){
            base.OnLoad(e);
            GL.ClearColor(1.0f, 1.0f, 1.0f, 0.0f);
            GL.Enable(EnableCap.DepthTest);
        }

        protected override void OnResize(EventArgs e){
            base.OnResize(e);
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 1.0f, 64.0f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);
        }

        protected override void OnUpdateFrame(FrameEventArgs e){
            base.OnUpdateFrame(e);
            if (Keyboard[Key.Escape])
                Exit();
            if(Keyboard[Key.A]){
                if(SampleStart - HorizontalPanFactor < 0)
                    SampleStart = 0;
                else 
                    SampleStart -= HorizontalPanFactor;
            }
            if(Keyboard[Key.D]){
                SampleStart += HorizontalPanFactor;
            }
            if(Keyboard[Key.Left]){
                DistanceBetweenSamples -= 0.0001f;
            }
            if(Keyboard[Key.Right]){
                DistanceBetweenSamples += 0.0002f;
            }
            if(Keyboard[Key.L]){
                Console.WriteLine(SampleStart);
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Scale(-1, 1, 1);
            Matrix4 modelview = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelview);

            GL.LineWidth(2);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(0.6f, 0.6f, 0.6f); 
            GL.Vertex3(-2.2f, 0, 4.0f);
            GL.Vertex3(3.0f, 0, 4.0f);
            GL.End();

            Visualize();

            SwapBuffers();
        }

        private void Visualize(){

            //bool SampleMarkersShown = false;

            foreach(var Input in Inputs){
                GL.LineWidth(1.5f);
                float StartX = -2.2f;                
                //visualize each waveforms if the current frame is within the waveforms' bounds
                if(SampleStart < Input.Samples.Length){
                    for(long i = SampleStart, j = 0; j < SamplesPerGlance; j++){
                        if(Input.ShowSampleBars && i + 1 < Input.Samples.Length && j + 1 < SamplesPerGlance){
                            GL.Color3(0.9f, 0.9f, 0.9f);
                            GL.Begin(PrimitiveType.Lines);
                            GL.Vertex3(StartX, Input.Samples[i] * WaveFormAmpFactor, 4.0f);
                            GL.Vertex3(StartX, 0, 4.0f);
                            GL.End();
                        }
                        /*if(SampleMarkers[i] && !SampleMarkersShown){
                            GL.Color3(0.7f, 0.5f, 0.3f);
                            GL.Begin(PrimitiveType.Lines);
                            GL.Vertex3(StartX, Input.Samples[i] * WaveFormAmpFactor, 4.0f);
                            GL.Vertex3(StartX, 0, 4.0f);
                            GL.End();
                        } 
                        */      
                        if(i + 1 < Input.Samples.Length && j + 1 < SamplesPerGlance){
                            GL.Color3(Input.WaveColor);
                            GL.Begin(PrimitiveType.Lines);
                            GL.Vertex3(StartX, Input.Samples[i] * WaveFormAmpFactor, 4.0f);
                            GL.Vertex3(StartX + DistanceBetweenSamples, Input.Samples[i + 1] * WaveFormAmpFactor, 4.0f); 
                            GL.End();
                        }
                        StartX += DistanceBetweenSamples;
                        i++;
                    }
                }
                //SampleMarkersShown = true;
            }
        }
    }

    public class VisualizerInput{
        public float[] Samples;
        public Color WaveColor;
        public bool ShowSampleBars;
        public VisualizerInput(float[] s, Color wc, bool sb = false){ Samples = s; WaveColor = wc; ShowSampleBars = sb; }
    }
}