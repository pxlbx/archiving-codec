using WavLib;
using System;

namespace Helpers{

    class FileHelper{

        //  retrieves the audio data needed by the ARC encoder from a WAVE file
        public static AudioData GetAudioData(string file){
            Loader loader = new Loader (file);
            AudioData a = new AudioData();

            a.FilePath = file;
            a.Samples = loader._samplesArray;
            a.BitsPerSample = loader._bitsPerSample;
            a.NumberOfChannels = loader._channels;
            a.SamplingRate = loader._sampleRate;

            return a;
        }

        public static void PrintAudioInfo(AudioData a){
            Console.WriteLine("No. of Samples: \t" + a.Samples.Length);
            Console.WriteLine("No. of Channels: \t" + a.NumberOfChannels);
            Console.WriteLine("Bit depth: \t\t" + a.BitsPerSample + " bits/sample");
            Console.WriteLine("Sampling Rate: \t\t" + a.SamplingRate + " Hz");
        }

    }

    class AudioData{
        public short[] Samples;
        public ushort NumberOfChannels;
        public ushort BitsPerSample;
        public uint SamplingRate;
        public string FilePath;
    }

}