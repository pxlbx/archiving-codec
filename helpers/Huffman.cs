using System.Collections.Generic;

namespace Helpers
{
    class HuffmanTree{
        public short Value = 0;
        public ulong Distribution = 0;
        public HuffmanTree Left, Right;
        public string PostFixExpression;
        public HuffmanTree(short v, HuffmanTree l = null, HuffmanTree r = null){
            Value = v;
            Left = l;
            Right = r;
        }
        public HuffmanTree(short v, ulong d, HuffmanTree l = null, HuffmanTree r = null){
            Value = v;
            Distribution = d;
            Left = l;
            Right = r;
            PostFixExpression = v.ToString();
        }
    }

    class HuffmanCoder{

        public HuffmanTree MakeHuffmanTree(ulong[] sk_distribution, short pfm){

            List<HuffmanTree> Nodes = new List<HuffmanTree>();

            for(short i = 0; i < sk_distribution.Length; i++){
                if(sk_distribution[i] != 0){
                    Nodes.Add(new HuffmanTree(i, sk_distribution[i]));
                }
            }
            return MakeHuffmanTree(Nodes ,pfm);
        }
        public HuffmanTree MakeHuffmanTree(List<HuffmanTree> Nodes, short pfm){
            // Merge trees until only one tree is left
            while(Nodes.Count > 1){
                var t1 = GetMinimum(Nodes); 
                var t2 = GetMinimum(Nodes);
                var root = new HuffmanTree(pfm, t1.Distribution + t2.Distribution);
                if(t1.Distribution >= t2.Distribution){
                    root.Left = t2;
                    root.Right = t1;
                    root.PostFixExpression = t2.PostFixExpression + " " + t1.PostFixExpression + " " + root.PostFixExpression;
                }
                else{
                    root.Left = t1;
                    root.Right = t2;
                    root.PostFixExpression = t1.PostFixExpression + " " + t2.PostFixExpression + " " + root.PostFixExpression;
                }
                Nodes.Add(root);
            }
            HuffmanTree htree = Nodes[0];
            return htree;
        }
        public HuffmanTree HuffmanTreeFromPostFix(byte[] PostFixExpression, byte PostFixMerger){
            Stack<HuffmanTree> postfix_stack = new Stack<HuffmanTree>();
            string postfix = "";
            foreach(var node_value in PostFixExpression){
                if(node_value != PostFixMerger){
                    //push the node to stack
                    postfix_stack.Push(new HuffmanTree(node_value));
                }
                else{
                    //merge the top 2 nodes of stack then push the parent node
                    HuffmanTree right = postfix_stack.Pop();
                    HuffmanTree left = postfix_stack.Pop();
                    postfix_stack.Push(new HuffmanTree(node_value, left, right));
                }
                postfix = postfix + " " + node_value.ToString();
            }
            var tree = postfix_stack.Pop();
            tree.PostFixExpression = postfix;
            return tree;
        }
        void GetHuffmanCodes(HuffmanTree t, string[] keys, string code = ""){
            if(IsLeaf(t)){
                keys[t.Value] = code;
            }
            else{
                GetHuffmanCodes(t.Left, keys, code + "0");
                GetHuffmanCodes(t.Right, keys, code + "1");
            }
        }
        public void GetHuffmanCodes(HuffmanTree t, string[] return_codes){
            for(int i = 0; i < return_codes.Length; i++)
                return_codes[i] = "";
            GetHuffmanCodes(t, return_codes, "");
        }
        bool IsLeaf(HuffmanTree ht){
            if(ht.Left == null && ht.Right == null)
                return true;
            return false;
        }
        HuffmanTree GetMinimum(List<HuffmanTree> t){
            int min_index = 0;
            ulong min_dist = t[0].Distribution;
            for(int i = 0; i < t.Count; i++){
                if(t[i].Distribution < min_dist){
                    min_index = i;
                    min_dist = t[i].Distribution;
                }
            }
            HuffmanTree min = t[min_index];
            t.RemoveAt(min_index);
            return min;
        }

    }
}
