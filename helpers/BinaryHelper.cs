using System;
using System.Linq;

namespace Helpers{
    class BinaryHelper{ 
        //  computes the length of the shortest binary representation of an unsigned integer
        public static byte SizeInBits(ulong n){
            return (byte)Math.Ceiling(Math.Log(n + 1, 2));
        }
        
        //  given an array of bits, returns the equivalent integer in base 10
        public static int BoolArrayToInt(bool[] bits){
            int num = 0;
            int exp = bits.Length - 1;
            foreach(var bit in bits){
                if(bit == true)
                    num += (int)Math.Pow(2, exp);
                exp--;
            }
            return num;
        }
    }

    //  adds a method to the integer data type for conversion
    //  to the integer's binary representation
    public static class UInt32Extensions{
        public static Boolean[] ToBooleanArray(this UInt32 i, int length){
            bool[] shortest_bin = Convert.ToString(i, 2).Select(s => s.Equals('1')).ToArray();
            if(shortest_bin.Length == length) return shortest_bin;
            if(shortest_bin.Length > length){
                Console.WriteLine("Bitlength not sufficient! " + i + " length: " + length);
                return null;
            }
            bool[] padded_bin = new bool[length];
            int diff = length - shortest_bin.Length;
            for(int c = diff; c < padded_bin.Length; c++){
                padded_bin[c] = shortest_bin[c - diff];
            }
            return padded_bin;
        }
    }
}