using System;
using System.Linq;
using OpenTK;
using System.Collections.Generic;
using WaveVisualizer;

namespace Helpers{
    class NumHelper{

        /* Converts a 16-bit integer array (short) to integer array */
        public static int[] ToIntArr(short[] arr){
            int[] iarr = arr.Select(x => (int)x).ToArray();
            return iarr;
        }

        /* Converts an integer array into a float array of normalized values. */
        /* The default normalization factor is 2^15, which is used for visualizing audio samples */
        public static float[] ToFloatArr(int[] arr, float normal_factor_exp = 16.0f){
            float[] floatarr = new float[arr.Length];
            double normal_factor = Math.Pow(2, normal_factor_exp);
            for(int i = 0; i < arr.Length; i++){
                floatarr[i] = (float)(arr[i] / normal_factor);
            }
            return floatarr;
        }

        /* The set of waveform colors for the optional waveform visualizer. */
        static Color[] default_colors = {Color.DarkCyan, Color.Crimson, Color.DarkMagenta, Color.Pink, Color.DarkSlateGray};

        /* Visualizes a set of waveforms (arrays of normalized float values)  using OpenTK */
        /* This uses the custom visualizer utility in Visualizer.cs */
        public static void Visualize(List<float[]> data){
            using(Visualizer viz = new Visualizer()){
                int c = 0;
                for(int i = 0; i < data.Count; i++){
                    viz.Inputs.Add(new VisualizerInput(data[i], default_colors[c], true));
                    c++;
                }
                viz.WaveFormAmpFactor = 3;
                viz.X = 200;
                viz.Y = 200;
                viz.Run();
            }
        }


    }
}