﻿using System;
using System.Diagnostics;
using System.IO;

namespace ARC{
    public class Program{
        //  available commands: Encode, Decode 
        const byte COMMAND_ENCODE = 1, COMMAND_DECODE = 2, COMMAND_INVALID = 3;
        public static void Main(string[] args){
            if(ExecuteCommand(args) == COMMAND_INVALID)
                Console.WriteLine("Invalid command.");
        }

        static byte ExecuteCommand(string[] args){
            if(args.Length != 2)
                return COMMAND_INVALID;
            
            //  parsing of commands
            if(args[0] == "-encode" || args[0] == "-e"){
                Encode(args[1]);
                return COMMAND_ENCODE;
            }
            if(args[0] == "-decode" || args[0] == "-d"){
                Decode(args[1]);
                return COMMAND_DECODE;
            }

            return COMMAND_INVALID;
        }

        static void Encode(string directory){
            //  Opens the specified directory
            DirectoryInfo d = new DirectoryInfo(directory);
            //  retrieves the WAVE files inside the directory
            FileInfo[] Files = d.GetFiles("*.wav");

            //  timer instance
            Stopwatch timer = new Stopwatch();
            foreach(FileInfo file in Files ){
                //  create instance of the ARC coder
                Coder c = new Coder();
                timer.Start();
                c.Compress(directory, file.Name);
                timer.Stop();
                Console.WriteLine("Encode Time: " + (timer.ElapsedMilliseconds / (double)1000) + " s");
                timer.Reset();
            }
        }

        static void Decode(string directory){
            //  Opens the specified directory
            DirectoryInfo d = new DirectoryInfo(directory);
            //  retrieves the ARC files inside the directory
            FileInfo[] Files = d.GetFiles("*.arc");

            //  timer instance
            Stopwatch timer = new Stopwatch();
            foreach(FileInfo file in Files ){
                //  create instance of the ARC decoder
                Decoder dec = new Decoder();
                timer.Start();
                dec.Decompress(directory, file.Name);
                timer.Stop();
                Console.WriteLine("Decode Time: " + (timer.ElapsedMilliseconds / (double)1000) + " s");
                timer.Reset();
            }
        }
    }


}
